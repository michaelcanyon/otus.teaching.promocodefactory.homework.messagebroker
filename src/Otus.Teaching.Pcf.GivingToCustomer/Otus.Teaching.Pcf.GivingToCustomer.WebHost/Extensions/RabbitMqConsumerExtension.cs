﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost
{
    public static class RabbitMqConsumerExtension
    {
        private static RabbitConsumer _listener { get; set; }

        public static IApplicationBuilder UseRabbitListener(this IApplicationBuilder app,
                                                            string queueTitle,
                                                            string hostName,
                                                            string login,
                                                            string password)
        {
            _listener = app.ApplicationServices.GetService<RabbitConsumer>();

            var lifetime = app.ApplicationServices.GetService<IApplicationLifetime>();

            lifetime.ApplicationStarted.Register(() => _listener.Register(queueTitle, hostName, login, password));

            return app;
        }
    }

}
