﻿using Otus.Teaching.Pcf.GivingToCustomer.Core.Interfaces;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Diagnostics;
using System.Text;
using System.Text.Json;
using System.Threading;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost
{
    public class RabbitConsumer
    {
        private readonly IPromocodeService _promocodeService;
        public RabbitConsumer(IPromocodeService promocodeService)
            => _promocodeService = promocodeService ?? throw new ArgumentNullException(nameof(promocodeService));

        public void Register(string queueTitle, string hostname, string userName, string password)
        {
            var factory = new ConnectionFactory() { HostName = hostname, UserName = userName, Password = password };

            using (var connection = factory.CreateConnection())
            {
                using (var channel = connection.CreateModel())
                {
                    channel.BasicQos(0, 10, false);
                    channel.QueueDeclare(queueTitle, false, false, false, null);

                    var consumer = new EventingBasicConsumer(channel);

                    consumer.Received += (sender, e) =>
                    {
                        Debug.WriteLine($"{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.ff")} Received message");
                        Thread.Sleep(TimeSpan.FromSeconds(2));
                        var body = e.Body;
                        var promocodeRequest = JsonSerializer.Deserialize<GivePromoCodeRequest>(Encoding.UTF8.GetString(body.ToArray()));
                        _promocodeService.GivePromoCodesToCustomersWithPreferenceAsync(
                            new Core.Models.GivePromoCodeDTO
                            {
                                BeginDate = promocodeRequest.BeginDate,
                                EndDate = promocodeRequest.EndDate,
                                PartnerId = promocodeRequest.PartnerId,
                                PreferenceId = promocodeRequest.PreferenceId,
                                PromoCode = promocodeRequest.PromoCode,
                                PromoCodeId = promocodeRequest.PromoCodeId,
                                ServiceInfo = promocodeRequest.ServiceInfo
                            });
                        channel.BasicAck(e.DeliveryTag, false);
                    };

                    channel.BasicConsume(queueTitle, false, consumer);

                    Debug.WriteLine($"Subscribed to the queue");

                    Console.ReadLine();
                }
            }
        }
    }
}
