﻿using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Interfaces;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Mappers;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Services
{
    public class PromocodeService : IPromocodeService
    {
        private readonly IRepository<PromoCode> _promocodeRepository;
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;
        public PromocodeService(IRepository<PromoCode> promocodeRepository, IRepository<Customer> customerRepository, IRepository<Preference> preferenceRepository)
        {
            _promocodeRepository = promocodeRepository ?? throw new ArgumentNullException(nameof(promocodeRepository));
            _customerRepository = customerRepository ?? throw new ArgumentNullException(nameof(customerRepository));
            _preferenceRepository = preferenceRepository ?? throw new ArgumentNullException(nameof(preferenceRepository));
        }

        public async Task GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeDTO request)
        {
            //Получаем предпочтение по имени
            var preference = await _preferenceRepository.GetByIdAsync(request.PreferenceId);

            if (preference == null)
                return;

            //  Получаем клиентов с этим предпочтением:
            var customers = await _customerRepository
                .GetWhere(d => d.Preferences.Any(x =>
                    x.Preference.Id == preference.Id));

            PromoCode promoCode = PromoCodeMapper.MapFromModel(request, preference, customers);

            await _promocodeRepository.AddAsync(promoCode);
        }
    }
}
