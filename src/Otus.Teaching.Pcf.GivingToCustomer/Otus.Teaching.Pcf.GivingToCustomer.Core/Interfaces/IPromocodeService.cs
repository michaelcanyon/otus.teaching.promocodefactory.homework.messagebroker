﻿using Otus.Teaching.Pcf.GivingToCustomer.Core.Models;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Interfaces
{
    public interface IPromocodeService
    {
        Task GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeDTO request);
    }
}
