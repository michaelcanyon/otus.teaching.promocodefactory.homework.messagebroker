﻿using System;
using System.Threading.Tasks;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Gateways;
using Otus.Teaching.Pcf.ReceivingFromPartner.WebHost.RabbitClient;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration
{
    public class NotificationGateway
        : INotificationGateway
    {
        private readonly IRabbitMqService _rabbitMqService;

        public NotificationGateway(IRabbitMqService rabbitMqService)
            => _rabbitMqService = rabbitMqService ?? throw new ArgumentNullException(nameof(rabbitMqService));

        public Task SendNotificationToPartnerAsync(Guid partnerId, string message)
        {
            _rabbitMqService.SendMessage(new { PartnerId = partnerId, Message = message }, "notificationsQueue");
            //Код, который вызывает сервис отправки уведомлений партнеру
            
            return Task.CompletedTask;
        }
    }
}