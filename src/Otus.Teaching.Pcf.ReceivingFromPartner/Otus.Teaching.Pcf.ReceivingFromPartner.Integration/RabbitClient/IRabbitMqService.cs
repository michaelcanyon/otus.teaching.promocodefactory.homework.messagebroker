﻿namespace Otus.Teaching.Pcf.ReceivingFromPartner.WebHost.RabbitClient
{
    public interface IRabbitMqService
    {
        void SendMessage(object obj, string queueTitle);

        void SendMessage(string message, string queueTitle);
    }
}
