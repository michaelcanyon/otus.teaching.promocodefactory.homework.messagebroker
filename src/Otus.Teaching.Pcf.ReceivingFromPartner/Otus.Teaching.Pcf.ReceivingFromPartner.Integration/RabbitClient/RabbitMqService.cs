﻿using RabbitMQ.Client;
using System.Text;
using System.Text.Json;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.WebHost.RabbitClient
{
    public class RabbitMqService : IRabbitMqService
    {
        public void SendMessage(object obj, string queueTitle)
            => SendMessage(JsonSerializer.Serialize(obj), queueTitle);

        public void SendMessage(string message, string queueTitle)
        {
            var factory = new ConnectionFactory() { HostName = "rabbitmqhost", UserName = "admin", Password = "admin" };
            using (var connection = factory.CreateConnection())
            {
                using (var channel = connection.CreateModel())
                {
                    channel.QueueDeclare(queue: queueTitle,
                                         durable: false,
                                         exclusive: false,
                                         autoDelete: false,
                                         arguments: null);

                    var body = Encoding.UTF8.GetBytes(message);
                    channel.BasicPublish(exchange: "",
                                         routingKey: queueTitle,
                                         basicProperties: null,
                                         body: body);
                }
            }
        }
    }
}
