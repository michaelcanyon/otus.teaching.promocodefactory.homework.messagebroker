﻿using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Gateways;
using Otus.Teaching.Pcf.ReceivingFromPartner.WebHost.RabbitClient;
using System;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration
{
    public class AdministrationGateway
        : IAdministrationGateway
    {
        private readonly IRabbitMqService _rabbitMqService;

        public AdministrationGateway(IRabbitMqService rabbitMqService)
            => _rabbitMqService = rabbitMqService ?? throw new ArgumentNullException(nameof(rabbitMqService));

        public Task NotifyAdminAboutPartnerManagerPromoCode(Guid partnerManagerId)
        {
            _rabbitMqService.SendMessage(partnerManagerId, "appliedPromocodesQueue");

            return Task.CompletedTask;
        }
    }
}