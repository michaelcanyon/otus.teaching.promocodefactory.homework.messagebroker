﻿using Otus.Teaching.Pcf.Administration.Core.Interfaces;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Diagnostics;
using System.Text;
using System.Text.Json;
using System.Threading;

namespace Otus.Teaching.Pcf.Administration.WebHost
{
    public class RabbitConsumer
    {
        private readonly IEmployeesService _employeeService;
        public RabbitConsumer(IEmployeesService employeesService)
            => _employeeService = employeesService ?? throw new ArgumentNullException(nameof(employeesService));

        public void Register(string queueTitle, string hostname, string userName, string password)
        {
            var factory = new ConnectionFactory() { HostName = hostname, UserName = userName, Password = password };

            using (var connection = factory.CreateConnection())
            {
                using (var channel = connection.CreateModel())
                {
                    channel.BasicQos(0, 10, false);
                    channel.QueueDeclare(queueTitle, false, false, false, null);

                    var consumer = new EventingBasicConsumer(channel);

                    consumer.Received += (sender, e) =>
                    {
                        Debug.WriteLine($"{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.ff")} Received message");
                        Thread.Sleep(TimeSpan.FromSeconds(2));
                        var body = e.Body;
                        var employeeId = JsonSerializer.Deserialize<Guid>(Encoding.UTF8.GetString(body.ToArray()));
                        _employeeService.UpdateAppliedPromocodesAsync(employeeId);
                        channel.BasicAck(e.DeliveryTag, false);
                    };

                    channel.BasicConsume(queueTitle, false, consumer);

                    Debug.WriteLine($"Subscribed to the queue");

                    Console.ReadLine();
                }
            }
        }
    }
}
