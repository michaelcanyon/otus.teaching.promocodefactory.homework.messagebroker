﻿using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using Otus.Teaching.Pcf.Administration.Core.Interfaces;
using System;
using System.Diagnostics;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.Core.Services
{
    public class EmployeesService : IEmployeesService
    {
        private readonly IRepository<Employee> _employeesRepository;

        public EmployeesService(IRepository<Employee> employeesRepository)
        {
            _employeesRepository = employeesRepository ?? throw new ArgumentNullException(nameof(employeesRepository));
        }

        public async Task UpdateAppliedPromocodesAsync(Guid id)
        {
            var employee = await _employeesRepository.GetByIdAsync(id);

            if (employee == null)
            {
                Debug.WriteLine("Сотрудник не найден");
                return;
            }

            employee.AppliedPromocodesCount++;

            await _employeesRepository.UpdateAsync(employee);
        }
    }
}
