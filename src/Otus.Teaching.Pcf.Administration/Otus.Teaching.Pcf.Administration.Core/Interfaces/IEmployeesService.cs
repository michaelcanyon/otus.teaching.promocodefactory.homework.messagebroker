﻿using System;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.Core.Interfaces
{
    public interface IEmployeesService
    {
        Task UpdateAppliedPromocodesAsync(Guid id);
    }
}
